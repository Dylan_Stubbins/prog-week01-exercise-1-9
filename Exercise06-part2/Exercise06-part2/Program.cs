﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06_part2
{
    class Program
    {
        static void Main(string[] args)
        {
            const double kilometers = 1.609344;
            var miles = 0;

            Console.WriteLine("Hello there. Enter the miles you would like converted into kilometers");
            miles = int.Parse(Console.ReadLine());

            Console.WriteLine($"That is {kilometers*miles} kilometers, thankyou");
        }
    }
}
