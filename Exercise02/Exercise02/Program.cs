﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Dylan";
            Console.WriteLine($"Hello {myName}, how are you?");
        }
    }
}
