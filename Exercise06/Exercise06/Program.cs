﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06
{
    class Program
    {
        static void Main(string[] args)
        {
            const double miles = 0.621371;
            var kilometers = 0;

            Console.WriteLine("Hello there. Enter the kilometers you would like converted into miles");
            kilometers = int.Parse(Console.ReadLine());

            Console.WriteLine($"That is {miles * kilometers} miles, thankyou");

        }
    }
}
