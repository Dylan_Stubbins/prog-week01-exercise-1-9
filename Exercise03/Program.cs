﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise03
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "";
           
            Console.WriteLine("Hello, what is your name?");
            myName = Console.ReadLine();

            Console.WriteLine($"Thankyou {myName}, have a good day.");
        }
    }
}
